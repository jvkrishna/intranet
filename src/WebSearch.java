import ai.intranet.constants.IntranetConstants;
import ai.intranet.constants.IntranetConstants.SearchType;

/**
 * Main Application Usage : java WebSearch <directory-name> <search-type>
 * 
 * @author Vamsi Krishna J
 *
 */
public class WebSearch {
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("You must provide the directoryName and searchStrategyName.  Please try again.");
		} else {
			String directoryName = args[0];
			String searchStrategyName = args[1];
			SearchType searchType = SearchType.getSearchType(searchStrategyName);
			if (searchType != null) {
				searchType.getSearchObj().doSearch(directoryName, IntranetConstants.START_NODE);
			} else {
				System.out.println("The valid search strategies are:");
				System.out.println("  BREADTH DEPTH BEST BEAM");
			}
		}

	}
}
