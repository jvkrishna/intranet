import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import ai.intranet.constants.IntranetConstants;
import ai.intranet.constants.IntranetConstants.SearchType;
import ai.intranet.dto.SearchNode;

/**
 * Task 2 : Real WWW Search
 * 
 * @author Vamsi Krishna J
 *
 */
public class WWWSearch {

	public static void main(String[] args) throws IOException {
		String searchTerm = IntranetConstants.DEFAULT_SEARCH_TERM;
		String searchDomain = IntranetConstants.DEFAULT_SERACH_DOMAIN;
		List<String> filterDomains = IntranetConstants.DEFAULT_FILTER_DOMAINS;
		SearchType searchType = SearchType.BREADTH;
		// Validations are not added
		if (args.length > 0) {
			String searchStrategyName = args[0];
			searchType = SearchType.getSearchType(searchStrategyName);
		}
		if (args.length >= 3) {
			searchDomain = args[1];
			searchTerm = args[2];
			if (args.length > 3) {
				filterDomains = new ArrayList<>();
				for (int i = 3; i < args.length; i++) {
					filterDomains.add(args[i]);
				}
			}

		}
		//Explicitly setting search domain
		IntranetConstants.SEARCH_DOMAIN = searchDomain;
		List<Pattern> filterPatterns = new ArrayList<>();
		for (String domain : filterDomains) {
			Pattern pattern = Pattern.compile(domain);
			filterPatterns.add(pattern);
		}
		if (searchType == null) {
			System.out.println(
					"Invalid search Stragey: Valid Usage \n java  -jar WWWSearch <BREADTH | DEPTH | BEST | BEAM> <search-domain> <searchTerm> [filters]");
		}

		searchType.getSearchObj().doWWWSearch(new SearchNode(searchDomain, null), searchTerm, filterPatterns);
	}

}
