package ai.intranet.processors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ai.intranet.dto.SearchNode;
import ai.intranet.utils.Utilities;

public class DepthFirstSearch implements Search {

	private int nodesVisited = 0;

	/**
	 * Search implemented using DFS.
	 */
	@Override
	public void doSearch(String directoryName, SearchNode startNode) {
		Deque<SearchNode> openedNodes = new ArrayDeque<SearchNode>();
		Set<SearchNode> closedNodes = new HashSet<SearchNode>();
		openedNodes.push(startNode);
		while (!openedNodes.isEmpty()) {
			SearchNode currentNode = openedNodes.pop();
			nodesVisited++;
			String contents = Utilities.getFileContents(directoryName + File.separator + currentNode.getNodeName());
			if (Utilities.isGoalNode(contents)) {
				Utilities.printSearchPath(currentNode);
				System.out.println(" Visited " + nodesVisited + " nodes, starting @" + " " + directoryName
						+ File.separator + startNode + ", using: Depth First Search.");
				break;
			}
			closedNodes.add(currentNode);
			List<SearchNode> childNodes = Utilities.getChildNodes(currentNode, contents);
			for (SearchNode childNode : childNodes) {
				if (!closedNodes.contains(childNode) && !openedNodes.contains(childNode)) {
					openedNodes.push(childNode);
				}
			}
		}

	}

	public void doWWWSearch(SearchNode startNode, String searchTerm, List<Pattern> filters) {
		Deque<SearchNode> openedNodes = new ArrayDeque<SearchNode>();
		Set<SearchNode> closedNodes = new HashSet<SearchNode>();
		openedNodes.push(startNode);
		while (!openedNodes.isEmpty()) {
			try {

				SearchNode currentNode = openedNodes.pop();
				System.out.println("Visiting :" + currentNode);
				nodesVisited++;
				Document doc = Jsoup.connect(currentNode.getNodeName()).get();
				String contents = doc.html();
				if (Utilities.isGoalNode(contents, searchTerm)) {
					Utilities.printSearchPath(currentNode);
					System.out.println(" Visited " + nodesVisited + " nodes, starting @" + " " + startNode
							+ File.separator + startNode + ", using: Breadth First Search.");
					break;
				}
				closedNodes.add(currentNode);
				List<SearchNode> childNodes = Utilities.getChildNodes(currentNode, doc, filters);
				for (SearchNode childNode : childNodes) {
					if (!closedNodes.contains(childNode) && !openedNodes.contains(childNode)) {
						openedNodes.push(childNode);
					}
				}
			} catch (IOException e) {
				// Ignoring exception
				e.printStackTrace();
			}

		}
	}

}
