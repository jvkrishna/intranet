package ai.intranet.processors;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import ai.intranet.dto.SearchNode;
import ai.intranet.utils.Utilities;

public class BestFirstSearch implements Search {

	private int nodesVisited = 0;

	@Override
	public void doSearch(String directoryName, SearchNode startNode) {
		Queue<SearchNode> openedNodes = new PriorityQueue<>();
		Set<SearchNode> closedNodes = new HashSet<SearchNode>();
		openedNodes.add(startNode);
		while (!openedNodes.isEmpty()) {
			SearchNode currentNode = openedNodes.poll();
			nodesVisited++;
			//System.out.println("Visiting " + currentNode);
			String contents = Utilities.getFileContents(directoryName + File.separator + currentNode.getNodeName());
			if (Utilities.isGoalNode(contents)) {
				Utilities.printSearchPath(currentNode);
				System.out.println(" Visited " + nodesVisited + " nodes, starting @" + " " + directoryName
						+ File.separator + startNode + ", using: Best First Search.");
				break;
			}
			closedNodes.add(currentNode);
			Map<String, String> htmlMap = Utilities.getHyperLinkAndText(contents);
			for (Entry<String, String> entrySet : htmlMap.entrySet()) {
				SearchNode childNode = new SearchNode(entrySet.getKey(), currentNode);
				childNode.setLinkScore(Utilities.getLinkScore(entrySet.getValue(), contents));
				if (!closedNodes.contains(childNode) && !openedNodes.contains(childNode)) {
					openedNodes.add(childNode);
				} else if (openedNodes.contains(childNode)) {
					// If node is already in the opened list, check scores and
					// update the list.
					Iterator<SearchNode> iterator = openedNodes.iterator();
					SearchNode matchedNode = null;
					while (iterator.hasNext()) {
						SearchNode existingNode = iterator.next();
						if (childNode.equals(existingNode)) {
							matchedNode = existingNode;
							break;
						}
					}
					if (childNode.getLinkScore() > matchedNode.getLinkScore()) {
						openedNodes.remove(matchedNode);
						openedNodes.add(childNode);
					}
				}
			}
		}

	}

	public void doWWWSearch(SearchNode startNode, String searchTerm, List<Pattern> filters) {
		Queue<SearchNode> openedNodes = new PriorityQueue<>();
		Set<SearchNode> closedNodes = new HashSet<SearchNode>();
		openedNodes.add(startNode);
		while (!openedNodes.isEmpty()) {
			try {
				SearchNode currentNode = openedNodes.poll();
				System.out.println("Visiting :" + currentNode);
				nodesVisited++;
				Document doc = Jsoup.connect(currentNode.getNodeName()).get();
				String contents = doc.html();
				if (Utilities.isGoalNode(contents, searchTerm)) {
					Utilities.printSearchPath(currentNode);
					System.out.println(" Visited " + nodesVisited + " nodes, starting @" + " " + startNode
							+ File.separator + startNode + ", using: Best First Search.");
					break;
				}
				closedNodes.add(currentNode);
				Map<String, String> htmlMap = Utilities.getHyperLinkAndText(doc, filters);
				for (Entry<String, String> entrySet : htmlMap.entrySet()) {
					SearchNode childNode = new SearchNode(entrySet.getKey(), currentNode);
					childNode.setLinkScore(
							Utilities.getLinkScore(entrySet.getKey(), entrySet.getValue(), contents, searchTerm));
					if (!closedNodes.contains(childNode) && !openedNodes.contains(childNode)) {
						openedNodes.add(childNode);
					} else if (openedNodes.contains(childNode)) {
						// If node is already in the opened list, check scores
						// and
						// update the list.
						Iterator<SearchNode> iterator = openedNodes.iterator();
						SearchNode matchedNode = null;
						while (iterator.hasNext()) {
							SearchNode existingNode = iterator.next();
							if (childNode.equals(existingNode)) {
								matchedNode = existingNode;
								break;
							}
						}
						if (childNode.getLinkScore() > matchedNode.getLinkScore()) {
							openedNodes.remove(matchedNode);
							openedNodes.add(childNode);
						}
					}
				}
			} catch (IOException e) {
				// Do nothing
				e.printStackTrace();
			}
		}

	}

}
