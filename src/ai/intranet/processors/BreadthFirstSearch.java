package ai.intranet.processors;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ai.intranet.dto.SearchNode;
import ai.intranet.utils.Utilities;

/**
 * Search strategy implemented using Breadth First Search.
 * 
 * @author Vamsi Krishna J
 *
 */
public class BreadthFirstSearch implements Search {

	private int nodesVisited = 0;

	/**
	 * Search implemented using BFS. Doing the goal test only after selected for
	 * expansion.
	 */
	@Override
	public void doSearch(String directoryName, SearchNode startNode) {
		Queue<SearchNode> openedNodes = new LinkedList<SearchNode>();
		Set<SearchNode> closedNodes = new HashSet<SearchNode>();
		openedNodes.add(startNode);
		while (!openedNodes.isEmpty()) {
			SearchNode currentNode = openedNodes.poll();
			nodesVisited++;
			String contents = Utilities.getFileContents(directoryName + File.separator + currentNode.getNodeName());
			if (Utilities.isGoalNode(contents)) {
				Utilities.printSearchPath(currentNode);
				System.out.println(" Visited " + nodesVisited + " nodes, starting @" + " " + directoryName
						+ File.separator + startNode + ", using: Breadth First Search.");
				break;
			}
			closedNodes.add(currentNode);
			List<SearchNode> childNodes = Utilities.getChildNodes(currentNode, contents);
			for (SearchNode childNode : childNodes) {
				if (!closedNodes.contains(childNode) && !openedNodes.contains(childNode)) {
					openedNodes.add(childNode);
				}
			}
		}

	}

	public void doWWWSearch(SearchNode startNode, String searchTerm, List<Pattern> filters) {
		Queue<SearchNode> openedNodes = new LinkedList<SearchNode>();
		Set<SearchNode> closedNodes = new HashSet<SearchNode>();
		openedNodes.add(startNode);
		while (!openedNodes.isEmpty()) {
			try {
				SearchNode currentNode = openedNodes.poll();
				System.out.println("Visiting :" + currentNode);
				nodesVisited++;
				Document doc = Jsoup.connect(currentNode.getNodeName()).get();
				String contents = doc.html();
				if (Utilities.isGoalNode(contents, searchTerm)) {
					Utilities.printSearchPath(currentNode);
					System.out.println(" Visited " + nodesVisited + " nodes, starting @" + " " + startNode
							+ File.separator + startNode + ", using: Breadth First Search.");
					break;
				}
				closedNodes.add(currentNode);
				List<SearchNode> childNodes = Utilities.getChildNodes(currentNode, doc, filters);
				for (SearchNode childNode : childNodes) {
					if (!closedNodes.contains(childNode) && !openedNodes.contains(childNode)) {
						openedNodes.add(childNode);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
