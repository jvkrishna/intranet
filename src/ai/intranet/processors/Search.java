package ai.intranet.processors;

import java.util.List;
import java.util.regex.Pattern;

import ai.intranet.dto.SearchNode;

/**
 * Search Interface
 * 
 * @author Vamsi Krishna J
 *
 */
public interface Search {

	/**
	 * Perform Search.
	 * 
	 * @param directoryName
	 */
	public void doSearch(String directoryName, SearchNode startNode);
	
	
	/**
	 * Perform real WWW search
	 * @param startNode
	 * @param searchTerm
	 * @param filters
	 */
	public void doWWWSearch(SearchNode startNode, String searchTerm, List<Pattern> filters);
}
