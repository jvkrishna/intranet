package ai.intranet.dto;

import java.io.Serializable;

import ai.intranet.constants.IntranetConstants.NodeStatus;

/**
 * Search Node data structure.
 * 
 * @author Vamsi Krishna J
 *
 */
public class SearchNode implements Serializable, Comparable<SearchNode> {

	private static final long serialVersionUID = -5048946638502251886L;

	private String nodeName;
	private NodeStatus nodeStatus;
	private SearchNode parentNode;
	private Double linkScore = 0d;

	public SearchNode(String nodeName, SearchNode parentNode) {
		this(nodeName, NodeStatus.UNEXPLORED, parentNode);
	}

	public SearchNode(String nodeName, NodeStatus nodeStatus, SearchNode parentNode) {
		super();
		this.nodeName = nodeName;
		this.nodeStatus = nodeStatus;
		this.parentNode = parentNode;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public NodeStatus getNodeStatus() {
		return nodeStatus;
	}

	public void setNodeStatus(NodeStatus nodeStatus) {
		this.nodeStatus = nodeStatus;
	}

	public SearchNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(SearchNode parentNode) {
		this.parentNode = parentNode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeName == null) ? 0 : nodeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchNode other = (SearchNode) obj;
		if (nodeName == null) {
			if (other.nodeName != null)
				return false;
		} else if (!nodeName.equals(other.nodeName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getNodeName();
	}

	public Double getLinkScore() {
		return linkScore;
	}

	public void setLinkScore(Double linkScore) {
		this.linkScore = linkScore;
	}

	@Override
	public int compareTo(SearchNode o) {
		return o.linkScore.compareTo(linkScore);
	}

}
