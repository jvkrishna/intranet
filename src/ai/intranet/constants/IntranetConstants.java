package ai.intranet.constants;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import ai.intranet.dto.SearchNode;
import ai.intranet.processors.BeamSearch;
import ai.intranet.processors.BestFirstSearch;
import ai.intranet.processors.BreadthFirstSearch;
import ai.intranet.processors.DepthFirstSearch;
import ai.intranet.processors.Search;

public class IntranetConstants {
	public static final SearchNode START_NODE = new SearchNode("page1.html", null);
	public static final String GOAL_PATTERN = "QUERY1 QUERY2 QUERY3 QUERY4";
	public static final Pattern LINK_PATTERN = Pattern.compile("(<A HREF =) (page[\\d]*.html)( >)([\\s\\w]*)(</A>)");
	
	//WWW Search Constants
	public static final String DEFAULT_SEARCH_TERM ="Vamsi Krishna";
	public static final String DEFAULT_SERACH_DOMAIN = "http://www.cs.iastate.edu";
	public static final List<String> DEFAULT_FILTER_DOMAINS = Arrays.asList("www.cs.iastate.edu","^(\\/\\S+)");
	//Value will be updated in main Method
	public static String SEARCH_DOMAIN;
	
	

	public enum NodeStatus {
		OPEN, CLOSED, UNEXPLORED;
	}

	public enum SearchType {
		BREADTH(new BreadthFirstSearch()), DEPTH(new DepthFirstSearch()), BEST(new BestFirstSearch()), BEAM(
				new BeamSearch());
		private Search searchObj;

		private SearchType(Search searchObj) {
			this.searchObj = searchObj;
		}

		public Search getSearchObj() {
			return searchObj;
		}

		public static SearchType getSearchType(String searchType) {
			for (SearchType searchTypeEnum : values()) {
				if (searchTypeEnum.toString().equalsIgnoreCase(searchType)) {
					return searchTypeEnum;
				}
			}
			return null;
		}

	}

}
