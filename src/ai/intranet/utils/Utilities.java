package ai.intranet.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ai.intranet.constants.IntranetConstants;
import ai.intranet.dto.SearchNode;

public class Utilities {

	/**
	 * get file content
	 * 
	 * @param fileName
	 * @return String
	 */
	public static synchronized String getFileContents(String fileName) {
		File file = new File(fileName);
		String results = null;
		try {
			int length = (int) file.length(), bytesRead;
			byte byteArray[] = new byte[length];

			ByteArrayOutputStream bytesBuffer = new ByteArrayOutputStream(length);
			FileInputStream inputStream = new FileInputStream(file);
			bytesRead = inputStream.read(byteArray);
			bytesBuffer.write(byteArray, 0, bytesRead);
			inputStream.close();

			results = bytesBuffer.toString();
		} catch (IOException e) {
			System.out.println("Exception in getFileContents(" + fileName + "), msg=" + e);
		}

		return results;
	}

	/**
	 * Create child nodes for a given page.
	 * 
	 * @param contents
	 * @return {@link List}
	 */
	public static List<SearchNode> getChildNodes(SearchNode parentNode, String contents) {
		List<SearchNode> childs = new LinkedList<SearchNode>();
		List<String> links = getPageLinks(contents);
		for (String link : links) {
			childs.add(new SearchNode(link, parentNode));
		}
		return childs;
	}

	/**
	 * Create child nodes for a given page.
	 * 
	 * @param contents
	 * @return {@link List}
	 */
	public static List<SearchNode> getChildNodes(SearchNode parentNode, Document doc, List<Pattern> filterPatterns) {
		List<SearchNode> childs = new LinkedList<SearchNode>();
		List<String> links = getPageLinks(doc, filterPatterns);
		for (String link : links) {
			if (link.startsWith("/")) {
				link = IntranetConstants.SEARCH_DOMAIN + link;
			}
			childs.add(new SearchNode(link, parentNode));
		}
		return childs;
	}

	/**
	 * 
	 * @param content
	 * @return {@link Boolean}
	 */
	public static Boolean isGoalNode(String contents) {
		return isGoalNode(contents,IntranetConstants.GOAL_PATTERN);
	}

	/**
	 * 
	 * @param content
	 * @return {@link Boolean}
	 */
	public static Boolean isGoalNode(String contents, String searchTerm) {
		return (contents != null && contents.indexOf(searchTerm) >= 0);
	}

	public static List<String> getPageLinks(Document doc, List<Pattern> filterPatterns) {
		List<String> hrefs = new ArrayList<>();
		Elements links = doc.getElementsByTag("a");
		for (Element link : links) {
			for (Pattern pattern : filterPatterns) {
				if (pattern.matcher(link.attr("href")).matches()) {
					hrefs.add(link.attr("href"));
					break;
				}
			}
		}
		return hrefs;
	}

	public static Map<String, String> getHyperLinkAndText(Document doc, List<Pattern> filterPatterns) {
		Map<String, String> map = new HashMap<>();
		Elements links = doc.getElementsByTag("a");
		for (Element link : links) {
			for (Pattern pattern : filterPatterns) {
				String linkStr = link.attr("href");
				if (pattern.matcher(linkStr).matches()) {
					if (linkStr.startsWith("/")) {
						linkStr = IntranetConstants.SEARCH_DOMAIN + linkStr;
					}
					map.put(linkStr, link.text());
					break;
				}
			}
		}
		return map;
	}

	public static double getLinkScore(String hyperLink, String hyperText, String contents, String searchTerm) {
		double score = 0;
		score += StringUtils.countMatches(hyperLink, searchTerm);
		score += StringUtils.countMatches(hyperText, searchTerm);
		score += StringUtils.countMatches(contents, searchTerm);
		return score;
	}

	public static List<String> getPageLinks(String contents) {
		List<String> links = new ArrayList<String>();
		Matcher matcher = IntranetConstants.LINK_PATTERN.matcher(contents);
		while (matcher.find()) {
			links.add(matcher.group(2));
		}
		return links;
	}

	public static Map<String, String> getHyperLinkAndText(String contents) {
		Map<String, String> map = new HashMap<>();
		Matcher matcher = IntranetConstants.LINK_PATTERN.matcher(contents);
		while (matcher.find()) {
			map.put(matcher.group(2), matcher.group(4));
		}
		return map;
	}

	/**
	 * Generates link scores in the range of [0-5]. Number of queries have
	 * assigned weight of 0.4 and order of queries will get 0.6 weight.
	 * 
	 * @param hyperText
	 * @return linkScore
	 */
	public static double getLinkScore(String hyperText, String contents) {
		double score = 0;
		Pattern queryPattern = Pattern.compile("QUERY(\\d)");
		Set<Integer> uniqueQueries = new HashSet<>();
		Pattern orderPattern = Pattern.compile("(QUERY\\d\\W)+");
		Matcher orderMatcher = orderPattern.matcher(hyperText);
		int maxOrderedQueries = 0;
		while (orderMatcher.find()) {
			List<Integer> orderedQueries = new ArrayList<>();
			Matcher matcher = queryPattern.matcher(orderMatcher.group());
			while (matcher.find()) {
				Integer queryNumber = Integer.valueOf(matcher.group(1));
				uniqueQueries.add(queryNumber);
				orderedQueries.add(queryNumber);
			}
			int i = 0, j = 1;
			if (maxOrderedQueries < 1) {
				maxOrderedQueries = 1;
			}
			while (j < orderedQueries.size()) {
				if (orderedQueries.get(j) > orderedQueries.get(j - 1)) {
					j++;
				} else {
					if (maxOrderedQueries < j - i) {
						maxOrderedQueries = j - i;
					}
					i = j;
					j++;
				}
			}
		}
		int numberOfQuries = uniqueQueries.size();
		int count = 0;
		Matcher matcher = queryPattern.matcher(contents);
		while (matcher.find()) {
			count++;
		}
		score = (0.6 * maxOrderedQueries) + (0.4 * numberOfQuries)
				+ (hyperText.trim().startsWith("QUERY") ? 1 : 0 + count / 100);
		return score;
	}

	/**
	 * Prints search path.
	 * 
	 * @param goalNode
	 */
	public static void printSearchPath(SearchNode goalNode) {
		System.out.println("Goal State : " + goalNode);
		Deque<SearchNode> deque = new ArrayDeque<SearchNode>();
		while (goalNode != null) {
			deque.push(goalNode);
			goalNode = goalNode.getParentNode();
		}
		System.out.println("Search Path Length is : " + deque.size());
		System.out.println("------- Search Path ------");
		while (!deque.isEmpty()) {
			System.out.print(deque.pop());
			if (deque.peek() != null)
				System.out.print(" --> ");
		}
		System.out.println();
	}

}
